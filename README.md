# Leirnes.no

![Bilde av Synneva og Johannes](./src/johannes_synneva.png)

Oversikt over ætten fra Leirnes

## Hvordan oppdatere slektstreet

1. Installer [Brothers Keeper](https://www.bkwin.org/bksetup7E.EXE)
2. Få tak i slektsdatabasen. Ligger på NASet
3. Åpne programmet, klikk på `Descendants tree chart` og søk fram `Synneva Bjordal`
4. Standard innstillinger ok, men man må kanskje endre skriftypen til feks Open Sans
5. Klikk `Start` og så gå på `File - Printer Setup` i menyen oppe
6. Velg `Microsoft Print to PDF`, sett `Paper size` til A3 og `Orientation` til `Landscape`
7. Fra menyen `File - Print` og lagre PDFen et lurt sted
8. Åpne PDFen i [GIMP](https://www.gimp.org/downloads/) og åpne hver side av PDFen som `images` (ikke som `layers`) og velg skalering på 200%
9. Klipp og lim sammen alle sidene til en stor fin side
10. Lagre som PNG og pass på at bildet ikke blir alt for stort i filstørrelse (rundt 500kb nå)
11. Putt det i git og commit

## webserver

nginx vhost : [leirnes.no](./leirnes.no)

vhost root :  `/var/www/leirnes.no/`

Symlink denne vhosten over til `sites-enabled`

```sh
sudo ln -s /home/fredrik/www/leirnes.no/leirnes.no /etc/nginx/sites-enabled/leirnes.no
```

Symlink `/src/` til webserveren-rooten

```sh
sudo ln -s /home/fredrik/www/leirnes.no/src/ /var/www/leirnes.no
```

## Oppdatere slektstreet med gramps

1. Åpne gramps og slektstreet
2. Marker Synneva Leirnes
3. Reports - Graphical reports - Descendant tree
4. Settings:

   - 10 generasjoner
   - 1 level of spouse
   - Hak vekk alle boksene
   - Ikke include border eller page number
   - `Scale tree to fit the size of the page` og `Resize page to fit tree size`
   - Descendant Display Format

     ```gramps
     $n
     (b. $b-{d. $d})
     ```

   - Spouse Display Format

     ```gramps
     $n
     (b. $b-{d. $d})
     ```

   - Ikke include marriage box
   - Inkluder notat med teksten `Oppdatert $T` og plasser i bottom left
   - Lagre som SVG med stilen `brothers-keeper-clone`(width=0 på CG2b-box. CG2-note-box, CG2-fam-box, line width=0.7 og farge #555753)
   - Transparent background

5. Lagre i `/home/fredrik/workspace/leirnes.no/src/etterkommere.svg`
6. Fiks SVGen som beskrevet i seksjonen "Fiks SVG"

## Fix SVG

Søk og erstatt i denne rekkefølgen

1. Erstatt ">, " med `>` (mangler etternavn)
2. Erstatt `-)<` med `)<` (mangler fødselsdato, kun år)
3. Erstatt `(b. )` med `` (vet ingenting om når de er født )
4. Erstatt `b.` med `f.` (engelsk - norsk)
5. Erstatt " <" med `<` (trailing whitespace)
